import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {HeaderIcon, HeaderLabel} from '../components/atoms';
import {icons} from '../constants';

const Home = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <HeaderIcon icon={icons.location} />
        <HeaderLabel />
        <HeaderIcon icon={icons.basket} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  header: {
    flexDirection: 'row',
  },
});

export default Home;
