import {CATEGORY_TYPE} from '../types/categoryTypes';
import {
  initialLocation,
  categoryData,
  restaurantData,
} from '../mock/category-mock';

const INITIAL_STATE = {
  location: initialLocation,
  categories: categoryData,
  restaurants: restaurantData,
};

const categoryReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CATEGORY_TYPE.SELECT_CATEGORY:
      return state;
    default:
      return state;
  }
};

export default categoryReducer;
