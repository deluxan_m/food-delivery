import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';

import {COLORS} from '../../constants';

const UnSelected = ({children, onPress}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={1}
      onPress={onPress}>
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 60,
    backgroundColor: COLORS.white,
  },
});

export default UnSelected;
