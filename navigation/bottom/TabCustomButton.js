import React from 'react';

import Selected from './Selected';
import UnSelected from './UnSelected';

const TabCustomButton = props => {
  const isSelected = props.accessibilityState.selected;

  return isSelected ? <Selected {...props} /> : <UnSelected {...props} />;
};

export default TabCustomButton;
