import React from 'react';
import {StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {Home, Search, Profile} from '../../screens';
import {icon, icons} from '../../constants';

import TabIcon from './TabIcon';
import TabCustomButton from './TabCustomButton';

const Tab = createBottomTabNavigator();

const Tabs = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: {
          elevation: 0,
          backgroundColor: 'transparent',
          borderTopWidth: 0,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <TabIcon focused={focused} icon={icons.cutlery} />
          ),
          tabBarButton: props => <TabCustomButton {...props} />,
        }}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: ({focused}) => (
            <TabIcon focused={focused} icon={icons.search} />
          ),
          tabBarButton: props => <TabCustomButton {...props} />,
        }}
      />
      <Tab.Screen
        name="Like"
        component={Search}
        options={{
          tabBarIcon: ({focused}) => (
            <TabIcon focused={focused} icon={icons.like} />
          ),
          tabBarButton: props => <TabCustomButton {...props} />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <TabIcon focused={focused} icon={icons.user} />
          ),
          tabBarButton: props => <TabCustomButton {...props} />,
        }}
      />
    </Tab.Navigator>
  );
};

export default Tabs;
