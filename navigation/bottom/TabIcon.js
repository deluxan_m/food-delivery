import React from 'react';
import {Image, StyleSheet} from 'react-native';

import {COLORS} from '../../constants';

const TabIcon = ({focused, icon}) => {
  return (
    <Image
      source={icon}
      resizeMode="contain"
      style={[
        styles.tabBarIcon,
        {tintColor: focused ? COLORS.primary : COLORS.secondary},
      ]}
    />
  );
};

const styles = StyleSheet.create({
  tabBarIcon: {
    width: 25,
    height: 25,
  },
});

export default TabIcon;
