import React from 'react';
import {Image, TouchableOpacity, StyleSheet} from 'react-native';

import {SIZES} from '../../constants';

const HeaderIcon = ({icon}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Image source={icon} style={styles.icon} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 50,
    paddingHorizontal: SIZES.padding * 2,
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
});

export default HeaderIcon;
