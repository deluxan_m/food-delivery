import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {COLORS, SIZES} from '../../constants';

const HeaderLabel = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Palaikuli, Nanattan, Mannar</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray,
    padding: SIZES.padding * 2,
    borderRadius: SIZES.radius,
  },
  text: {},
});

export default HeaderLabel;
